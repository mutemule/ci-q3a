FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install -y ioquake3 ioquake3-server tini && \
    rm -rf /var/lib/lists/* /tmp/* /var/cache/apt/*

RUN groupadd q3a && \
    useradd -c "Quake3 Arena" -d /var/empty -M -g q3a -s /sbin/nologin q3a

USER q3a

VOLUME [ "/usr/lib/ioquake3/baseq3" ]

EXPOSE 27960/udp

ENTRYPOINT [ "/usr/bin/tini", "--", "/usr/lib/ioquake3/ioq3ded", "+exec" ]
CMD [ "q3config.cfg" ]
