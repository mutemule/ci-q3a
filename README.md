# Container Image to build q3a

This builds a generic, map-free container for [Quake 3: Arena](https://ioquake3.org/).
Maps are not included and must be obtained separately, along with system
configuration.

# Objective

Most publicly-built container images tend to be bloated and insecure,
depending on services like S6 which pull in large base systems, and run
as root with the full Linux capabilities.

This image is small and lean, and is designed to be run requiring no special
privileges. Additionally, it comes with a fully functional `seccomp` profile,
further restricting the impact of a security concern.

Ussually it is strongly preferred to include configuration data within the
container, as this allows us to strictly version application behaviour, rather
than just the compiled binary. Since ioquake3 configuration is tied directly
to the available maps, this container excludes configuration files.

# Requirements

Using this container requires that you have a volume that you mount as
`/usr/lib/ioquake3/baseq3`. This will need to have not only all your maps,
but also the configuration file. A sample configuration is included in this
repository under `config/`.

A sample `seccomp` profile is provided, and would need to be copied to the host
that will be running this container. Following the example below, it's been
placed at `/etc/docker/seccomp/q3a.json`.

# Using

This will vary based on the containers that you use, but a straightforward
usage of the base container looks something like this:

```sh
docker run -d \
    --name q3a \
    --init \
    --volume baseq3:/usr/lib/ioquake3/baseq3 \
    --tmpfs /tmp \
    --publish 27960:27960/udp \
    --read-only \
    --cap-drop all \
    --security-opt no-new-privileges \
    --security-opt seccomp=/etc/docker/seccomp/q3a.json \
    --pids-limit 50 \
    registry.gitlab.com/mutemule/ci-q3a/main:latest > "/var/run/q3a.cid"
```
